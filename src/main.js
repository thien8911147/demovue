import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import store from './store/index'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@fortawesome/fontawesome-free/css/all.css'
import 'font-awesome/css/font-awesome.min.css'
import * as VueGoogleMaps from "vue2-google-maps";
import 'es6-promise/auto'
import 'vue-loading-overlay/dist/vue-loading.css';
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import {Howl, Howler} from 'howler'
// Vue.user(VueMaterial)
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDr1TrnUj7Y_WAENg7O-sA8_HPC3xnb4WA",
    libraries: "places,drawing,visualization"
  }
});

Vue.config.productionTip = false

new Vue({
  VueMaterial,
  Howl,
  Howler,
  icons: {
    iconfont: 'fa',
  },
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
