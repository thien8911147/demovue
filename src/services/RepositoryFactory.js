import UsersRepository from "./UsersRepository";

const repositories = {
    users: UsersRepository,
}

const RepositoryFactory = {
    get: name => repositories[name]
}

export default RepositoryFactory;