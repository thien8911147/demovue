import axios from "axios";

const baseUrl = process.env.VUE_APP_API_URL;
// const baseUrl = "https://jsonplaceholder.typicode.com"
const repository = axios.create({
    baseURL: baseUrl,
    headers: {
        "Content-Type": "application/json",
    }
});

export default repository;