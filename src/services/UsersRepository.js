import Repository from "./Repository";

const resource = "/users";

export default {
  getListUsers() {
    return Repository.get(`${resource}`);
  },
  getDetailUser(id) {
    return Repository.get(`${resource}/${id}`);
  },
}