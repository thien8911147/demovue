import Vue from 'vue'
import Vuex from 'vuex'
import {userLogin} from '../mockup/listMusic'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    check: false,
    userLogin:{
      username: 'admin',
      password: 'admin'
    },
    userInfo: {
      userId: "sssss",
      userName: "",
      post: "",
    },
    searchText: "",
    notice: {
      content: [],
      badges: 0
    },
    loading: 0,
  },
  getters: {
    loggedIn: state => {
      return Boolean(state.userInfo.userId.trim());
    }
  },
  mutations: {
    signin(state, userData){
      for(let i = 0; i < userLogin.length ; i++){
          if(userLogin[i].username == userData.username &&
             userLogin[i].password == userData.password){
                state.check = true;
                localStorage.setItem("Info", JSON.stringify({'username':userData.username}));
                localStorage.setItem("check", true);
                return
             }
      }
    },
    logOut(state){
      state.check = null
      localStorage.removeItem('check')
      localStorage.removeItem('Info')
      localStorage.removeItem('FB')
      localStorage.removeItem('token')
      localStorage.removeItem('userId')
      location.reload();
    },
    setCheck(state, value){
      localStorage.setItem("check", true);
      state.check = value;
    },
    setInfoLogin(state, userInfo){
      localStorage.setItem("Info", JSON.stringify({'username':userInfo}));
      state.userInfo.userName = userInfo
    },
    setUserInfo(state, userInfo) {
      state.userInfo.userId = userInfo.userId;
      state.userInfo.userName = userInfo.userName;
      state.userInfo.post = userInfo.post;
    },
    SET_SEARCH_TEXT: (state, text) => {
      state.searchText = text;
    },
    SET_NOTICE: (state, data) => {
      if(data){
        state.notice.content = [...data];
        state.notice.badges = data ? data.length : 0;
      } else {
        state.notice.content = [];
        state.notice.badges = 0;
      }
    },
    START_LOADING: state => state.loading++,
    FINISH_LOADING: state => {
        if(state.loading > 0) {
          state.loading-- 
        }
      },
  },
  actions: {
    setInfoLogin: ({commit}, data) =>{
      commit('setInfoLogin', data)
    },
    setSearchText: ({commit, state}, newValue) => {
      commit('SET_SEARCH_TEXT', newValue)
      return state.searchText
    },
    setNotice: ({commit}, data) => {
      commit('SET_NOTICE', data)
    },
    startLoading: ({commit}) => {
      commit('START_LOADING');
    },
    finishLoading: ({commit}) => {
      commit('FINISH_LOADING');
    }
  },
  modules: {
  },
})
