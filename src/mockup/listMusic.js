export const ListMusic = [
    {id: 1, title: "Streets of Sant'Ivo", artist: "Ask Again"},
    {id: 2, title: "Remember the Way", artist: "Badwas" },
    {id: 3, title: "Belive", artist: "BarBol"},
    {id: 4, title: "Remember Me", artist: "Don'tAgain" }
]

export const PlayList = [
    {title: "Streets of Sant'Ivo", artist: "Ask Again", howl: null, display: true},
    {title: "Remember the Way", artist: "Ask Again", howl: null, display: true},
    {title: "Guardians", artist: "Ask Again", howl: null, display: true},
    {title: "Crusade of The Castellan", artist: "Ask Again", howl: null, display: true},
    {title: "Land of a Folk Divided", artist: "Ask Again", howl: null, display: true},
    {title: "An Innocent Sword", artist: "Ask Again", howl: null, display: true}
]

export const ChartData = [
    ['2014', 1000, 400, 200],
    ['2015', 1170, 460, 250],
    ['2016', 660, 1120, 300],
    ['2017', 1030, 740, 350]
]

export const BarChart = [
    ['Hero','Value'],
    ['Master Yi', 1000],
    ['Veigar', 1170],
    ['Ezreal', 660],
    ['Teemo', 1030]
]

export const userLogin = [
    {
        username: 'admin',
        password: 'admin'
    },
    {
        username: 'thien',
        password: '123123'
    },
    {
        username: 'vip',
        password: '12345'
    },
]