import Vue from "vue"
import Router from "vue-router"
import Home from '../components/HelloWorld.vue'
import PageNull from '../components/PageNull.vue'
import ListData from '../components/ListData.vue'
import FormSubmit from '../components/Expand.vue'
import Detail from '../components/Detail.vue'
import Gchart from '../components/Gchart.vue'
import Login from '../components/Login.vue'
import FacebookLogin from '../components/FacebookLogin.vue'

Vue.use(Router)
const router = new Router({
  // base: "/" + process.env.VUE_APP_CLIENT_CONTEXT_NAME + "/",
  mode: "history", // URLはハッシュ形式「#～～」ではなく「/～～」とする
  linkActiveClass: "active",
  routes: [
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: {
        requiresAuth: true, // <-- add this meta flag against which you check later in beforeEach
      },
    },
    {
      path: '/',
      name: 'Home',
      query: { next: 'list-data' },
      component: Home,
      children: [
        {
          path: "list-data",
          name: "ListData",
          component: ListData
        },
        {
          path: "login-page",
          name: "FacebookLogin",
          component: FacebookLogin
        },
        {
          path: "form-submit",
          name: "FormSubmit",
          component: FormSubmit
        },
        {
          path: "detail",
          name: "Detail",
          component: Detail
        },
        {
          path: '/gchart',
          name: 'Gchart',
          component: Gchart,
        },
        {
          path: '/page-null',
          name: 'PageNull',
          query: { next: '' },
          component: PageNull,
        },
      ]
		},
	]
})

router.beforeEach((to, from, next) => {
  let check = localStorage.getItem('check')
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (to.name !== 'Login'){
      next({
        path: "/login",
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      next();
    }
  }else if(to.name == null){
    next({
      path: "/page-null",
      query: {
        redirect: "/page-null"
      }
    });
  } else {
    if(check != null && check != false){
      next();
    }else{
      next('/login');
    }
  }
});


export default router